title:  『君の名は。』My Dairy App 製作中
date: 2016-12-10 16:10:23
tags: kiminonwa
---
![](https://github.com/erttyy8821/MyDiary/raw/master/screenshot/s_0.png)

![](https://o3ziuzht9.qnssl.com/mydairy_app.png)

Workflow:

![](https://github.com/erttyy8821/MyDiary/raw/master/screenshot/usercase.png)

介面預覽圖：

![](https://o3ziuzht9.qnssl.com/mydairy_logo.png)

![](https://o3ziuzht9.qnssl.com/mydairy_splash.png)
  
![](https://o3ziuzht9.qnssl.com/sketchboard_mitsuha.png)

![](https://o3ziuzht9.qnssl.com/sketchboard_taki.png)

![](https://o3ziuzht9.qnssl.com/sketchboard_takiindex.png)

[![](https://o3ziuzht9.qnssl.com/iOS-store-badge.png)](https://itunes.apple.com/us/app/ni-ri-ji-ni-ming-zi-tong-kuan/id1183155138?ls=1&mt=8
)


<!-- more -->
