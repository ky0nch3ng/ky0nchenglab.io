title: Potplayer 美化版
tags:
  - Potplayer
id: 329

  - 項目
date: 2013-07-18 19:35:05
---

**Daum PotPlayer**（[朝鮮語](http://zh.wikipedia.org/wiki/%E6%9C%9D%E9%AE%AE%E8%AA%9E "朝鮮語")：**다음 팟플레이어**），是[KMPlayer](http://zh.wikipedia.org/wiki/The_KMPlayer "The KMPlayer")的原作者[姜勇囍](http://zh.wikipedia.org/w/index.php?title=%E5%A7%9C%E5%8B%87%E5%9B%8D&amp;action=edit&amp;redlink=1 "姜勇囍（頁面不存在）")進入[Daum](http://zh.wikipedia.org/wiki/Daum "Daum")公司後的新一代作品，目前正在全力開發中。由於採用[Delphi](http://zh.wikipedia.org/wiki/Delphi "Delphi")編譯程序的KMPlayer有一些弊端，姜龍喜為改進播放器本身的一些性能而重新用[VC++](http://zh.wikipedia.org/wiki/VC%2B%2B "VC++")進行構架。

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6r6yotuyaj20dc048q32.jpg)

由於PotPlayer與KMPlayer同屬一個開發者的產品，所以PotPlayer擁有KMPlayer的硬件加速、支持高清影片播放、界面整潔、操作簡單、使用輕鬆、皮膚豐富多彩等特點，能夠滿足不同用戶的使用需求。Daum PotPlayer也可以在64位元Windows系統上運行。

Potplayer支持多種格式的視頻和音頻，幾乎可以擔當一般的常用媒體的播放

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6r72lry8bj21410nsn10.jpg)

&nbsp;

支持更換皮膚，皮膚的特殊效果需要在設置中開啓特效！  [更多皮膚下載](http://browse.deviantart.com/?qh=&amp;section=&amp;global=1&amp;q=potplayer)

![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1e6r70lcxwuj20ke0el40y.jpg)

軟件設置方面更是有豐富的選項供用戶定製自己喜好播放模式

&nbsp;

[![](http://bcs.duapp.com/chromelab/blog/201307/download.png)](http://pan.baidu.com/s/1c0iVfOG)

&nbsp;

Copyleft 2013    圖標來自 [PLEX](http://browse.deviantart.com/art/Plex-287022406)
