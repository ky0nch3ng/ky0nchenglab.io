title: NetSpeedMonitor – 在任務欄上顯示當前網速
tags:
  - NetSpeedMonitor
id: 136

  
date: 2013-07-10 12:16:01
---

可以再任務欄上顯示當前網速（包括上傳/下載速度）

![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1e6hlj7jocsj20bt05amxl.jpg)

&nbsp;

下載地址：[http://pan.baidu.com/share/link?shareid=2780167763&amp;uk=18653279](http://pan.baidu.com/share/link?shareid=2780167763&amp;uk=18653279)

Windows8用戶  請設置兼容性

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6hll7vk7dj20c10g3jsz.jpg)
