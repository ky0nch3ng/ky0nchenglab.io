title: 支持Android 5.x下Chrome的標題欄沉浸顏色
date: 2015-01-29 00:37:50
tags:
  - android
  - chrome
  - lollipop
---

![](https://ssl.daoapp.io/ww2.sinaimg.cn/large/6d9bd6a5jw1eoppe8z8vaj20lc0zktbv.jpg)

此效果要求：Android版本5.x + Chrome 39+

![](https://ssl.daoapp.io/ww1.sinaimg.cn/mw690/6d9bd6a5jw1eoppe8c8kvj20lc0zk775.jpg)

後臺多任務效果是這樣的

代碼就一行 加在網頁的 `<head>` 標籤內  顏色選個你喜歡的就可以了

    <meta name="theme-color" content="#ffffff">

網站favicon推薦64x64的ico文件

    <link href="../favicon.ico" rel="shortcut icon" type="image/x-icon" />
