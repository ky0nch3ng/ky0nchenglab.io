title: 又一個好用的文本編輯器 Atom
date: 2015-06-16 19:38:47
tags: atom
---
![](https://o3ziuzht9.qnssl.com/atom_banner.jpg)


475天前  Github 發佈了 Atom 編輯器的第一個測試版本，基於Chromium Embedded Framework 也算是個 Web App 了。 2333

<!--more-->

現在 Atom 編輯器已經放出了1.0.0的穩定版本,相對於 Sublime，Atom 的啓動速度和處理代碼速度並不理想 在 Mac 平臺很流暢 或許是因爲 SSD 的關係，如果在 Windows 平臺，爲了保證相應的速度，建議還是放在 SSD 裏面運行。

Atom 項目地址 ： [Atom.io](https://atom.io)


如果你是前 Sublime Text 的用戶,換到Atom應該感覺變化不大，包括一些快捷鍵還是原來一樣的。

![](https://o3ziuzht9.qnssl.com/atom_prtsc.jpg)

擴展性和 Sublime 差不多，有很多插件可以選擇，不過安裝插件可以在 Atom 裏面操作也可以用 Terminal裏面使用 apm 安裝插件包

![](https://o3ziuzht9.qnssl.com/atom_plugins.jpg)

或者下載插件包解壓到 ~/.atom

![](https://o3ziuzht9.qnssl.com/atom_dir.jpg)

有幾個插件挺不錯的 可以安利一下

script：Atom 裏面運行代碼 支持很多程序語言
atom-beautify：代碼格式化工具
mini-map：類似 sublime 右邊的代碼縮略圖
monokai:sublime 的代碼高亮樣式

![](https://o3ziuzht9.qnssl.com/atom_packages.jpg)

用了一段時間的 VS CODE。  覺得測試版的問題太多了 還是換回 Atom 吧~
