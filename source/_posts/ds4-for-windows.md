title: DualShock 4 手柄有線/藍牙無線連接PC
date: 2015-11-09 15:41:32
tags: 
---
之前寫過一篇用ds4tool這個軟件連接windows的方法

現在又一個免費開源的連接軟件能幫助大法DualShock 4 手柄有線/藍牙無線連接PC

![](https://o3ziuzht9.qnssl.com/ds4windows.jpg)

<!--more-->

首先去 [ds4windows](http://ds4windows.com) 的網站下載最新的版本

下載到本地之後 打開 ds4windows

![](https://o3ziuzht9.qnssl.com/ds4index.jpg)

點擊 setting 裡面的 controller driver install 之後 按照提示安裝

![](https://o3ziuzht9.qnssl.com/ds4win_diver_install.png)

之後插上 USB 連接 PC 或者藍牙連接 都可以 Windows自己也會按照相應的手柄驅動

這部分的設置 見過去寫的原文 http://akarin.xyz/ps4-controller-on-pc/

成功連接自己的手柄 背後的LED會從白色變成藍色

![](https://o3ziuzht9.qnssl.com/ds4windows.png)

這個軟件也有很多的自定義設置  自己慢慢去發現吧

![](https://o3ziuzht9.qnssl.com/ds4settings.jpg)

索尼大法的手柄確實好用 雖然不用 PS4 玩遊戲 但是買一個連接自己的計算機玩遊戲不覺得更好嗎？

![](https://o3ziuzht9.qnssl.com/steambigpicture.jpg)

![](https://o3ziuzht9.qnssl.com/d833c895d143ad4bc695bc3984025aafa50f068b.jpg)
