title: Hexo 靜態博客使用指南
date: 2016-07-26 16:10:23
tags: hexo
---
![](https://o3ziuzht9.qnssl.com/hexo-banner.jpg)

上次我介紹了 Hexo 的基本搭建方法  近期 [tommy351](http://twitter.com/tommy351)大神 又更新了版本
和之前的 2.x 使用步驟略有不同 目前剛剛更新  可能有許多主題和插件不兼容 3.x  請等待後續更新

本文介紹從零開始搭建的方法 如需從 2.x 升級到 3.x 遷移方法請移至本文最後

<!-- more -->
# I.Hexo 簡介

Hexo 是一個輕量的靜態博客框架。通過Hexo可以快速生成一個靜態博客框架,僅需要幾條命令就可以完成,相當方便。

而架設Hexo的環境更簡單了  不需要 lnmp/lamp 這些繁瑣複雜的環境 僅僅需要一個簡單的http服務器即可使用 或者使用互聯網上免費的頁面託管服務

比如本人的這個博客 就是託管於 [Gitlab](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/) 的Pages服務上

* 本文會更隨 Hexo 的發展同步更新 如需請收藏本文 非常感謝您的閱讀! 如有錯誤,歡迎指出。


# II.Hexo 安裝方法

- Mac OS X/Linux或其他UNIX/類UNIX系統 配置 nodejs 環境
  - 請去[官方網站](http://nodejs.org/download/)下載 `.pkg` 文件 或者下載已編譯完成的二進制文件


  - 也可以下載源代碼編譯安裝

```bash
$ wget http://nodejs.org/dist/v0.12.4/node-v0.12.7.tar.gz
$ tar zxvf node-v0.12.7.tar.gz
$ cd node-v0.12.7
# ./configure --prefix=/usr
# make && make install
```

- npm(node 包管理器)

  - redhat 系

    ```bash
    # yum install npm -y
    ```

  - debian 系
    ```bash
    # apt-get install npm -y
    ```

其他發行版請自行尋找 wiki 獲得幫助

### 通過 npm 安裝 hexo-cli

```bash
$ npm install hexo-cli -g
```

如果以上命令不能安裝 可以嘗試把官方源替換爲 [淘寶 npm 源](https://npm.taobao.org/) 再執行安裝Hexo

```bash
$ npm install -g cnpm --registry=https://registry.npm.taobao.org
```

或者你直接通過添加 `npm` 參數 `alias` 一個新命令:

```bash
$ alias cnpm="npm --registry=https://registry.npm.taobao.org \
--cache=$HOME/.npm/.cache/cnpm \
--disturl=https://npm.taobao.org/dist \
--userconfig=$HOME/.cnpmrc"
```

如果以上的 cnpm 無法安裝 可以試試看下方的 alias 方法

```bash
$ echo '\n#alias for cnpm\nalias cnpm="npm --registry=https://registry.npm.taobao.org \
--cache=$HOME/.npm/.cache/cnpm \
--disturl=https://npm.taobao.org/dist \
--userconfig=$HOME/.cnpmrc"' >> ~/.zshrc && source ~/.zshrc
```

- Windows 系統（具體環境配置請參考[這個](http://www.cnblogs.com/seanlv/archive/2011/11/22/2258716.html)）
 - git
 - node 環境
 - npm(node 包管理器)


# III.Hexo 配置方法

新建一個需要當做博客目錄的文件夾

```bash
$ mkdir blog
```

進去之後加入 hexo 主程序和安裝 npm

```bash
$ hexo init && npm install
```

![](https://o3ziuzht9.qnssl.com/hexo_install.jpg)

文件夾大致結構如下

![](https://o3ziuzht9.qnssl.com/hexo_tree.jpg)

- scaffolds 工具模板

- scripts hexo的功能js

- source 博客資源文件夾

- source/_drafts 草稿文件夾

- source/_posts 文章文件夾

- themes 存放皮膚的文件夾

- themes/landscape 默認皮膚文件夾

- _config.yml 全局配置文件

- db.json json格式的靜態常量數據庫


`_posts`目錄：Hexo 存放博客文章的文件夾

`themes`目錄：存放皮膚的文件夾，默認使用官方的主題 你也可以從 [hexo主題頁面](https://hexo.io/themes/) 或 [hexo主題 wiki 頁面](https://github.com/tommy351/hexo/wiki/Themes)下載你喜歡的主題


----------
## 配置 Hexo

### Hexo 全局配置

用文本編輯器修改 `_config.yml` 這個文件 大致如下 只需要自行修改幾個 其他保持默認即可

通常需要修改站點名稱 /URL格式 /歸檔設置 /disqus評論用戶名 /部署配置 這幾項就可以了 注意冒號後面都要添加一個半角空格 之後纔是設置參數

自定義域名設置 在 `source` 我文件夾下面新建 `CNAME` 文件 裏面寫入你的自定義域名 並設置您的dns配置cname方式到服務提供商的給的地址即可

```conf
# 網站
參數	描述
title	網站標題
subtitle	網站副標題
description	網站描述
author	您的名字
language	網站使用的語言
timezone	網站時區。Hexo 預設使用您電腦的時區。時區列表

# 網址
參數	描述	默認值
url	網址
root	網站根目錄
permalink	文章的 永久鏈接 格式	:year/:month/:day/:title/
permalink_default	永久鏈接中各部分的默認值
網站存放在子目錄
如果您的網站存放在子目錄中，例如 http://yoursite.com/blog，則請將您的 url 設爲 http://yoursite.com/blog 並把 root 設爲 /blog/。

# 目錄
參數	描述	默認值
source_dir	資源文件夾，這個文件夾用來存放內容。	source
public_dir	公共文件夾，這個文件夾用於存放生成的站點文件。	public
tag_dir	標籤文件夾	tags
archive_dir	歸檔文件夾	archives
category_dir	分類文件夾	categories
code_dir	Include code 文件夾	downloads/code
i18n_dir	國際化（i18n）文件夾	:lang
skip_render	跳過指定文件的渲染，您可使用 glob 來配置路徑。

# 文章
參數	描述	默認值
new_post_name	新文章的文件名稱	:title.md
default_layout	預設佈局	post
auto_spacing	在中文和英文之間加入空格	false
titlecase	把標題轉換爲 title case	false
external_link	在新標籤中打開鏈接	true
filename_case	把文件名稱轉換爲 (1) 小寫或 (2) 大寫	0
render_drafts	顯示草稿	false
post_asset_folder	啓動 Asset 文件夾	false
relative_link	把鏈接改爲與根目錄的相對位址	false
future	顯示未來的文章	true
highlight	代碼塊的設置

# 分類 & 標籤
參數	描述	默認值
default_category	默認分類	uncategorized
category_map	分類別名
tag_map	標籤別名
日期 / 時間格式
Hexo 使用 Moment.js 來解析和顯示時間。

參數	描述	默認值
date_format	日期格式	MMM D YYYY
time_format	時間格式	H:mm:ss
分頁
參數	描述	默認值
per_page	每頁顯示的文章量 (0 = 關閉分頁功能)	10
pagination_dir	分頁目錄	page

# 擴展
參數	描述
theme	當前主題名稱
deploy	部署
```




### Hexo常用插件安裝與配置
安裝首頁文章數量  存檔  分類 的插件
安裝本地服務器代理插件
安裝發佈器插件
安裝更新插件 rss site-map之類的

```bash
$ npm install hexo-generator-index --save
$ npm install hexo-generator-archive --save
$ npm install hexo-generator-category --save
$ npm install hexo-generator-tag --save
$ npm install hexo-server --save
$ npm install hexo-deployer-git --save
$ npm install hexo-deployer-heroku --save
$ npm install hexo-deployer-rsync --save
$ npm install hexo-deployer-openshift --save
$ npm install hexo-renderer-marked@0.2.10 --save
$ npm install hexo-renderer-stylus@0.3.1 --save
$ npm install hexo-generator-feed@1.1.0 --save
$ npm install hexo-generator-sitemap@1.1.2 --save
```

裝完之後去全局配置文件 `_config.yml` 修改參數

```conf
index_generator:
  per_page: 10 ##首頁默認10篇文章標題 如果值爲0不分頁

archive_generator:
  per_page: 10 ##歸檔頁面默認10篇文章標題
  yearly: true  ##生成年視圖
  monthly: true ##生成月視圖

tag_generator:
  per_page: 10 ##標籤分類頁面默認10篇文章

category_generator:
   per_page: 10 ###分類頁面默認10篇文章

feed:
  type: atom ##feed類型 atom或者rss2
  path: atom.xml ##feed路徑
  limit: 20  ##feed文章最小數量


deploy:
  type: git ##部署類型 其他類型自行google之
  repo: <repository url> ##git倉庫地址
  branch: [branch] ##git 頁面分支
  message: [message] ##git message建議默認字段update 可以自定義

-多部署

deploy:
  type: git
  message: update  ##git message建議默認字段update 可以自定義
  repo:
  gitcafe: <repository url>,[branch] ##gitcafe 倉庫地址和分支
  github: <repository url>,[branch] ##github 倉庫地址和分支
```

例如

```bash
deploy:
    type: git
    message: "update"
    repo:
    gitcafe: https://gitcafe.com/ky0ncheng/ky0ncheng.git,gitcafe-pages
    github: https://github.com/ky0ncheng/ky0ncheng.github.io.git,master
```

注意:

* Github Pages 和 Gitlab Pages 服務 分支都在 master

* Gitlab Pages 需要啟用 Gitlab CI 服務, `deploy` 字段需注释掉。每次 push 到 gitlab 仓库前须在本地生成静态页面(hexo g)。

純文本網頁配置參數存於 `.gitlab-ci.yml`

```conf
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master
```

Hexo 配置參數存於 `.gitlab-ci.yml` ,令注意 Hexo 的配置文件 `_config.yml` 內 `deploy` 段請注釋掉。
Deploy 請把整個目錄 push 至 gitlab 倉庫的 master 分支上。 

```conf
image: node:4.2.2

pages:
  cache:
    paths:
    - node_modules/

  script:
  - npm install hexo-cli -g
  - npm install
  - hexo deploy
  artifacts:
    paths:
    - public
  only:
    - master
```

更多插件可以去 Hexo 插件 wiki 找到 https://github.com/hexojs/hexo/wiki/Plugins


### Hexo 主題設置

同樣編輯主題文件夾的 `_config.yml`

```conf
# Header
menu:#導航欄連接
Home: /
Archives: /archives #歸檔頁面URL
自定義頁面標題: /自定義頁面URL
rss: /atom.xml  #rss地址  默認即可

# Content
excerpt_link: Read More #閱讀更多的文字顯示
fancybox: true #開啓fancybox效果

# Sidebar  #側邊欄設置
sidebar: right
widgets:
- category
- tag
- tagcloud
- archive
- recent_posts

# Miscellaneous #社交網絡和統計連接地址
google_analytics: #google analytics ID
favicon: /favicon.png #網站的favicon
twitter:
google_plus:
fb_admins:
fb_app_id:


網站 Favicon 設置

```html
<link rel="short icon" href="/favicon.png">
```

--------
## 寫文章
Hexo 使用 markdown 語法的純文本存放文章 後綴爲 `.md` 你可以在 `_post` 文件夾裏面新建這個後綴的 `.md` 文件  使用的全是 UTF-8 編碼

也可以輸入命令以生成

```bash
$ hexo new <title>
```

如果是新建一個頁面

```bash
$ hexo new page <title>
```

看一下剛纔生成的<title>.md，內容如下：

```conf
 title: title #文章標題
 date: 2015-02-05 12:47:44 #文章生成時間
  #文章分類目錄 可以省略
 tags: #文章標籤 可以省略
 description: #你對本頁的描述 可以省略
 ---
 這裡開始使用 markdown 格式輸入你的正文。

多標籤注意語法格式 如下:

tags:
- 標籤1
- 標籤2
- 標籤3
- etc...

想在首頁文章預覽添加圖片可以添加 photo 參數 這個 fancybox 可以省略 如下:

```markdown
photos:
  - http://xxx.com/photo.jpg
```

正文中可以使用 `<!--more-->` 設置文章摘要 如下:

以上是文章摘要
```html
<!--more-->
```
以下是餘下全文

more 以上內容即是文章摘要，在主頁顯示，more 以下內容點擊『> Read More』鏈接打開全文才顯示。


如果您曾經是 Wordpress 的用戶可以使用 hexo 的插件把之前的文章進行轉換成 md 的無格式文件

1. 先從 Wordpress 後臺導出所有的文章為 XML 格式.

2. 安裝 hexo 的插件 hexo-migrator-wordpress

```bash
$ npm install hexo-migrator-wordpress --save
```

3. 使用 hexo-migrator-wordpress 轉換 xml 為 md 文件 (export.xml 請換成您的具體文件名)

```bash
$ hexo migrate wordpress export.xml
```

命令執行後 您會在 `source/_post` 目錄內就可以看到剛剛轉換完成的博客文章 md 文件



--------
# IV.Hexo 部署方法

寫完文章之後 就可以啓動本地服務器測試了

```bash
$ hexo s
```

這個時候 hexo 啓動 localhost 的 4000 端口 靜態的網站架設完成
![](https://o3ziuzht9.qnssl.com/hexo_server.jpg)

修改後就可以部署上去了

```bash
$ hexo clean #清除緩存
$ hexo g #生成靜態網頁
$ hexo d #開始部署
```


---------
# V.Hexo更新方法

```bash
$ npm update -g
```

* cnpm 源也許會同步官方源失敗 造成 hexo 無法更新到最新版本

* hexo 同時也需要最新的 nodejs

```bash
$ npm install n -g
# n stable
```

以上就是 Hexo 的基本使用方法  進階的可以在 Hexo 的官方文檔裡找到  感謝閱讀!

Enjoy~


--------

Hexo 3.x 帶來了更好的性能，更新了API，模塊化了大部分組件。更多變化看這裏： https://github.com/hexojs/hexo/wiki/Breaking-Changes-in-Hexo-3.0

Hexo2.x到3.x遷移指南：

原文： https://github.com/hexojs/hexo/wiki/Migrating-from-2.x-to-3.08
