title: 開源的廣告攔截工具 µBlock
id: 2219

  
date: 2014-08-09 03:35:22
tags:
---

一款基於Chromium瀏覽器的高效廣告攔截工具，超低的CPU與內存使用。 一款高效的廣告攔截工具：超低的內存和CPU佔用，和其他常見的廣告攔截工具相比，可以加載並執行上千條過濾規則。 ![](http://juanzii.me/1/wp-content/uploads/2014/08/ublock1.png) 效率概述說明： [https://github.com/gorhill/uBlock/wiki/%C2%B5Block-vs.-ABP:-efficiency-compared](https://github.com/gorhill/uBlock/wiki/%C2%B5Block-vs.-ABP:-efficiency-compared)

用法：點擊彈出窗口中的電源按鈕，µBlock將對於當前網頁永久禁用/啓用廣告攔截功能。 它只適用於當前網頁，而不是全局按鈕。

Chrome商店地址 [https://chrome.google.com/webstore/detail/%C2%B5block/cjpalhdlnbpafiamejdnhcphjbkeiagm](https://chrome.google.com/webstore/detail/%C2%B5block/cjpalhdlnbpafiamejdnhcphjbkeiagm) <!--more-->

它不只是一個廣告攔截工具，它還可以從hosts文件裏讀取和創建過濾規則。 初始默認加載和執行下列過濾規則： -EasyList -EasyPrivacy - Malware domains - Long-lived malware domains - Peter Lowe’s Ad server list - Malware Domains List 這裏還擁有更多的過濾規則供你選擇：

*   Fanboy’s Enhanced Tracking List
*   Dan Pollock’s hosts file
*   hpHosts’s Ad and tracking servers
*   MVPS HOSTS
*   Spam404
*   Etc. 啓用越多的過濾規則就會產生越多的內存佔用。 然而，即使在添加 Fanboy 額外的兩個規則和 hpHosts’s Ad and tracking servers，µBlock 的內存佔用依然比其他常見的過濾工具要小的多。 另外，請注意選擇的一些額外的列表可能會導致網頁破損可能性增高 — — 尤其是那些通常用作hosts文件的列表。
*   *   *沒有這些過濾規則列表，這個擴展就沒有了意義。 所以如果你真的想要做點兒貢獻，想想那些努力維護廣告過濾規則列表的人們，至少這一切可以免費使用。

*   *   *免費。 開放源代碼與公共許可證 (GPLv3) 一切爲了用戶。 貢獻者@ Github：
[https://github.com/gorhill/uBlock/graphs/contributors](https://github.com/gorhill/uBlock/graphs/contributors) 貢獻者 @ Crowdin： [https://crowdin.net/project/ublock](https://crowdin.net/project/ublock) * * *它是一個非常早期的版本，當你評論的時候記住這一點。 項目更改日誌：

[https://github.com/gorhill/uBlock/wiki/Change-log](https://github.com/gorhill/uBlock/wiki/Change-log) * * *大陸境內推薦設置 勾選Easylisty China

![](http://juanzii.me/1/wp-content/uploads/2014/08/ublockget.png)
此擴展自定義過濾語法同 ABP
