---
title: 解決 Android Nougat 7.1.1 的狀態欄感嘆號問題
date: 2017-02-02 06:12:46
tags:
---
首先下載對應平台 Google 的 [SDK Platform Tools](https://developer.android.com/studio/releases/platform-tools.html)

```
adb shell
settings put global captive_portal_https_url https://ky0n.xyz/generate_204
reboot
```

本文參考：

[關於 Android 5.0-7.1.1 網絡圖標上的感嘆號及其解決辦法](https://www.noisyfox.cn/android-captive-portal.html)
[在 Android 7.0 上解決感嘆號問題 \[更新至7.1.1\]](https://mine260309.me/archives/1587)
[消除 Android 7.1.1 中 Wi-Fi 和移動網絡上的 x 號 丨一日一技 · Android](http://sspai.com/37009)

<!--more-->