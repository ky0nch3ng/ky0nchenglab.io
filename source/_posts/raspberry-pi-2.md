title: 樹莓派2上手
date: 2015-03-22 11:35:45
tags: rasberrypi
---
![](http://ww4.sinaimg.cn/large/6d9bd6a5gw1eqecwrgxl9j20go0b3wfk.jpg)

第二代樹莓派（Raspberry Pi 2）於2015/02/03正式發佈，同現有的Model  B+售價相同，爲$35，用戶可以通過[element14](http://www.element14.com/community/community/raspberry-pi) 和 [RS Components](http://uk.rs-online.com/web/generalDisplay.html?id=raspberrypi)兩家合作商進行選購。樹莓派2採用了900MHz的四核ARM Cortex-A7處理器（性能是前代的6倍以上），1GB的LPDDR2 SDRAM（內存提升兩倍），並完美兼容第一代樹莓派。因爲採用了ARMv7處理器，所以能夠運行所有ARM GNU/Linux分支版本，包括[Snappy Ubuntu Core](http://www.raspberrypi.org/downloads/)和微軟的[Windows 10](http://dev.windows.com/en-us/featured/raspberrypi2support)系統。

<!-- more -->

全新的樹莓派2的硬件規格如下：

* 處理器：博通BCM2836（CPU, GPU, DSP, SDRAM和一個USB端口)
* CPU：900MHz的四核ARM Cortex A7（ARMv7架構）
* GPU：博通VideoCore IV @ 250 MHz
* OpenGL ES 2.0 (24 GFLOPS)
* 1080P 30 MPEG-2和VC-1解碼器 (帶授權證書)
* 1080p30的H.264 / MPEG - 4 AVC高調解碼器和編碼器
* 內存：1GB（同GPU共享）
* USB端口：4個
* 視頻輸入：15針的MIPI攝像頭接口（CSI）控制器
* 視頻輸出：HDMI、通過3.5mm套件的複合視頻（PAL和NTSC）
* 音頻輸出： I²S
* 音頻輸入：3.5mm耳機套件，通過HDMI和 I²S進行數字輸入
* 存儲：MicroSD卡
* 網絡：10/100 Mbit/s Ethernet
* 外設：17 GPIO加特殊功能，和HAT ID總線
* 電源:800 mA (4.0 W)
* 電源來源：通過MicroUSB或者GPIO頭的5V電
* 尺寸： 85.60 mm × 56.5 mm
* 重量：45g


新型樹莓派2 代B型板可兼容以往創建的所有樹莓派項目。同時，它還具備擴展GPIO引腳並可提供高級電源管理和連接性能，因而可連接多達4個USB設備，包括一些如硬盤驅動器的功率器件。其40針擴展GPIO接口使其能夠添加更多傳感器、連接器及擴展板，前26針引腳與A型板和B型板保持一致，可100%向後兼容。


<video width="680" height="480" controls>
<source src="https://git.oschina.net/kawaiiushio/cdn/raw/master/rspi2.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

我預訂了一個RS的腐國產的樹莓派2   順便入了一個亞克力殼和一個小風扇和兩個銅片散熱器

![](http://ww2.sinaimg.cn/large/6d9bd6a5gw1eqeoxq9bmvj21kw16o1jp.jpg)

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1eqep03ugqnj21kw16oqmk.jpg)

![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1eqep0d31nmj21kw16onls.jpg)

![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1eqeozi7o9gj21kw23uhdt.jpg)

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1eqeozv0hzgj21kw16oe28.jpg)


拿到手之後  在TF卡上燒了官方的debain發行版做引導系統  

![](http://ww2.sinaimg.cn/large/6d9bd6a5gw1eqep6xhmfaj212o0niqik.jpg)

什麼都沒開的系統佔用

![](http://ww1.sinaimg.cn/large/6d9bd6a5gw1eqep7kbcgmj212y0nch7b.jpg)


接下來準備搭個dns服務器負責整個寢室的地址解析  順便架個nginx


有時間再折騰吧~  XD
