title: 夏至南下
date: 2015-06-18 21:19:02
tags:
---
又是一年夏至 每年到了這個時候 天總是格外的熱 這次又正好出差南下 感覺心更累了 T-T

![](https://o3ziuzht9.qnssl.com/mf_tem.jpg)

<!--more-->

這次去深圳參加了一年一度的 Maker Faire 我也是第一次去 這次在展會上看到了好多有意思的東西

深圳這個城市也是第一次來 之前去過廣州 也是如此悶熱 我無法想象這個城市晚上沒有空調的日子2333

![](https://o3ziuzht9.qnssl.com/2015shenzhen.jpg)

會場上空的天很藍

![](https://o3ziuzht9.qnssl.com/sz_sky.jpg)

這次 Maker Faire 有許多大牌大廠 也有很多創業新項目

大疆的多軸飛行器在這次展會上也是出足了風頭  希望這家佔據世界90%的多軸飛行器廠商能走得更遠吧

關於此次的更多照片 我之後會上傳至在本人的 Flickr

--------------


![](https://o3ziuzht9.qnssl.com/sz-bay-bridge.jpg)

![](https://o3ziuzht9.qnssl.com/sz_to_hk_bus.jpg)

![](https://o3ziuzht9.qnssl.com/peking_rd.jpg)

香港最無聊的一條馬路 上面的小店基本都是中國人開的 東西專門買給大陸遊客

![](https://o3ziuzht9.qnssl.com/cqds.jpg)

旁邊的重慶大廈 門口都是來自非洲/印度/東南亞的基佬  不過這個基佬是真的 GAY 喜歡攪基的可以去找他們技術交♂流交♂流  

![](https://o3ziuzht9.qnssl.com/nightview_hk.jpg)



<div align=right>乙未年 夏至</div>  

<div align=right>記於深圳/香港/上海</div>  
