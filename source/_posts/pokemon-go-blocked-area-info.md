title: Pokemon GO 區域鎖定信息解釋
date: 2016-07-22 16:10:23
tags:
- pokemon
---

位於南韓東北部的小鎮束草（속초）本身只是個海邊小城鎮，距離首爾大概 2 小時巴士的行程就可以到達，對韓國人來說是個夏日去海灘的地方。不過最近因為 Pokemon GO 變得熱門，而束草是南韓（甚至亞洲區）唯一一個可以順利玩到 Pokemon GO 的地方，令不少人慕名而來，到這個小城市化身 Pokemon 大師。

![](https://ssl.daoapp.io/cdn.unwire.hk/wp-content/uploads/2016/07/CnQA_ucVYAA-zWm.jpg)

下面詳解一下 鎖區狂魔任天堂 和 Nientic 的 GPS 鎖定方式。

<!-- more -->
之前大陸網絡媒體所報導的位於紅框內的地區不能遊戲是錯誤的 GPS 劃分方式。

![](https://o3ziuzht9.qnssl.com/9df0006dc1aca83aac4.jpg)

按照 Nientic 方面表示，並沒有開放韓國地區的遊戲，但原來他們對於地區的分法（參照 Ingress ）是採用菱形分割形式，剛好覆蓋韓國的菱形區域中，把韓國東北面的一個地域分割了出來沒有包含在內，所以不受限制影響。其中束草就是在裏面人口較多的地方，擁有多個地標，自然就能大玩特玩。

![](https://o3ziuzht9.qnssl.com/P1-BY031_SPOKEM_16U_20160715152406.jpg)

![](https://o3ziuzht9.qnssl.com/koera-blocked.png)

眾所周知，Pokemon Go 遊戲中的地點數據同樣來自 Nientic 旗下的 [Ingress](https://www.ingress.com/)。
同理，香港地區目前也未開放遊戲權限，此時可以看一下香港地區的 GPS 地圖信息。

![](https://o3ziuzht9.qnssl.com/hk_area_blocked.png)

香港地區大約被劃分成了四個 GPS 菱形區域，因此在未來開放香港地區的同時廣東省的一些周邊城市也會因此受益。

如果遊戲區域真的是劃分成矩形，或許上海也能被日區解鎖受益23333。

![](https://o3ziuzht9.qnssl.com/sh_area_blocked.png)

離的太遠2333

坐等鎖區狂魔任天堂解鎖。

-------------------
港區已經解鎖，預測失敗，果然是任地獄的作風。

-------------------

附：

[Ingress 地圖數據(官方提供)](https://www.ingress.com/intel)

[Pokemon Go 的可玩地區信息](https://www.ispokemongoavailableyet.com/)

[Pokemon Go 圖鑒 on Google Docs](https://docs.google.com/spreadsheets/d/1sFYWFPFt6Yfm_IjSi1HNpx9irX6FlwWaz2UVWas4YKo/edit#gid=0)

[Pokemon Go 圖鑒](http://www.pokeuniv.com/bbs/forum.php?mod=viewthread&tid=7190)

[Pokemon Go 地圖數據](https://www.pokegoworld.com/)

[PoGoMap](https://jz6.github.io/PoGoMap/)

[repokemon](https://cheeaun.github.io/repokemon/)

新闻稿来源 [unwire.hk](http://unwire.hk)
