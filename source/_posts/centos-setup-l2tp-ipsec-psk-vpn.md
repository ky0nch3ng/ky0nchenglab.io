title: CentOS搭建L2TP / IPSec VPN教程
tags:
  - Linux
date: 2014-01-19 02:32:08
---

<div>建议使用最小化安装的centos纯净系统，此教程在centos 5.8和 centos 6.3上测试通过
首先确保关闭了selinux
导入第三方源</div>
<div></div>
<div></div>
<div>

centos 5.x 32位使用
<pre class="brush: bash; title: Bash">rpm -ivh http://mirror.karneval.cz/pub/linux/fedora/epel/5/i386/epel-release-5-4.noarch.rpm</pre>
centos 5.x 64位使用
<div>
<pre class="brush: bash; title: Bash">rpm -ivh http://mirror.karneval.cz/pub/linux/fedora/epel/5/x86_64/epel-release-5-4.noarch.rpm</pre>
</div>
centos 6.x 32位使用
<div>
<pre class="brush: bash; title: Bash">rpm -ivh http://mirror.karneval.cz/pub/linux/fedora/epel/6/i386/epel-release-6-8.noarch.rpm</pre>
</div>
centos 6.x 64位使用
<div>
<pre class="brush: bash; title: Bash">rpm -ivh http://mirror.karneval.cz/pub/linux/fedora/epel/6/x86_64/epel-release-6-8.noarch.rpm</pre>
</div>
开始安装
<div>
<pre class="brush: bash; title: Bash">yum install openswan xl2tpd ppp lsof iptables -y</pre>
</div>
编辑 /etc/sysctl.conf
<div>
<pre class="brush: bash; title: Bash">nano /etc/sysctl.conf</pre>
</div>
将
<pre class="brush: bash; title: Bash">net.ipv4.ip_forward = 0</pre>
修改为
<pre class="brush: bash; title: Bash">net.ipv4.ip_forward = 1</pre>
接下来依次执行
<pre class="lang:default decode:true">echo "net.ipv4.conf.all.accept_redirects = 0" |  tee -a /etc/sysctl.conf
echo "net.ipv4.conf.all.send_redirects = 0" |  tee -a /etc/sysctl.conf
for vpn in /proc/sys/net/ipv4/conf/*; do echo 0 &gt; $vpn/accept_redirects; echo 0 &gt; $vpn/send_redirects; done
sysctl -p</pre>
编辑 /etc/rc.local
<div>
<pre class="brush: bash; title: Bash">nano /etc/rc.local</pre>
</div>
在末尾加入
<pre class="lang:default decode:true  ">for vpn in /proc/sys/net/ipv4/conf/*; do echo 0 &gt; $vpn/accept_redirects; echo 0 &gt; $vpn/send_redirects; done</pre>
编辑 /etc/ipsec.conf
<div>
<pre class="brush: bash; title: Bash">nano /etc/ipsec.conf</pre>
</div>
将其中的

</div>
<div>
<pre class="brush: bash; title: Bash">virtual_private=</pre>
</div>
<div>

修改为
<pre class="brush: bash; title: Bash">virtual_private=%v4:10.0.0.0/8,%v4:192.168.1.0/24,%v4:172.16.0.0/12,%v6:fd00::/8,%v6:fe80::/10</pre>
并在文件末尾加入以下内容，注意将以下的111.111.111.111修改为你服务器 IP
<pre class="brush: bash; title: Bash">conn L2TP-PSK-NAT
   rightsubnet=vhost:%priv
   also=L2TP-PSK-noNAT

conn L2TP-PSK-noNAT
   authby=secret
   pfs=no
   auto=add
   keyingtries=5
   rekey=no
   ikelifetime=8h
   keylife=1h
   type=transport
   left=111.111.111.111
   leftprotoport=17/1701
   right=%any
   rightprotoport=17/%any</pre>
编辑 /etc/ipsec.secrets
<div>
<pre class="brush: bash; title: Bash">nano /etc/ipsec.secrets</pre>
</div>
清空这个文件的内容，然后添加
<pre class="brush: bash; title: Bash">111.111.111.111 %any: PSK "dilehost"</pre>
注意111.111.111.111请修改为你的服务器ip ,PSK 后为共享密钥，自行更改

启动ipsec
<div>
<pre class="brush: bash; title: Bash">service ipsec start</pre>
</div>
查看系统IPSec安装和启动的正确性：
<div>
<pre class="brush: bash; title: Bash">ipsec verify</pre>
</div>
没有出现 [FAILED] 就可以了

编辑/etc/xl2tpd/xl2tpd.conf
<div>
<pre class="brush: bash; title: Bash">nano /etc/xl2tpd/xl2tpd.conf</pre>
</div>
修改
<pre class="brush: bash; title: Bash">ip range = 192.168.1.128-192.168.1.254
local ip = 192.168.1.99</pre>
为
<pre class="brush: bash; title: Bash">ip range = 192.168.1.2-192.168.1.254
local ip = 192.168.1.1</pre>
编辑 /etc/ppp/options.xl2tpd
<div>
<pre class="brush: bash; title: Bash">nano /etc/ppp/options.xl2tpd</pre>
</div>
清空原有配置，加入以下配置内容
<pre class="brush: bash; title: Bash">require-mschap-v2
ms-dns 8.8.8.8
ms-dns 8.8.4.4
auth
mtu 1200
mru 1000
crtscts
hide-password
modem
name l2tpd
proxyarp
lcp-echo-interval 30
lcp-echo-failure 4</pre>
设置用户名密码及登录IP
<div>
<pre class="brush: bash; title: Bash">nano /etc/ppp/chap-secrets</pre>
</div>
加入一个用户，比如用户名为username ,密码为password , * 为任意IP都可以登录
<pre class="brush: bash; title: Bash">username    l2tpd     password      *</pre>
对应好就行，多个用户每行配置一个

重启服务并添加开机启动
<div>
<pre class="brush: bash; title: Bash">service ipsec restart
service xl2tpd restart
chkconfig ipsec on
chkconfig xl2tpd on</pre>
</div>
最后配置防火墙
<div>
<pre class="brush: bash; title: Bash">iptables -t nat -A POSTROUTING -s 192.168.1.0/24 -o eth0 -j MASQUERADE
iptables -I FORWARD -s 192.168.1.0/24 -j ACCEPT
iptables -I FORWARD -d 192.168.1.0/24 -j ACCEPT
iptables -A INPUT -p udp -m state --state NEW -m udp --dport 1701 -j ACCEPT
iptables -A INPUT -p udp -m state --state NEW -m udp --dport 500 -j ACCEPT
iptables -A INPUT -p udp -m state --state NEW -m udp --dport 4500 -j ACCEPT
/etc/rc.d/init.d/iptables save
/etc/rc.d/init.d/iptables restart</pre>
</div>
</div>
<div>参考：</div>
<div>[http://www.live-in.org/archives/818.html](http://www.live-in.org/archives/818.html)</div>
<div>[https://raymii.org/s/tutorials/IPSEC_L2TP_vpn_on_CentOS_-_Red_Hat_Enterprise_Linux_or_Scientific_-_Linux_6.html](https://raymii.org/s/tutorials/IPSEC_L2TP_vpn_on_CentOS_-_Red_Hat_Enterprise_Linux_or_Scientific_-_Linux_6.html)</div>