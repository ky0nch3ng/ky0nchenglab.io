title: 牆內 Google 開
date: 2016-03-10 22:51:29
tags: google nginx encrypt
---
![](https://o3ziuzht9.qnssl.com/wenlu.png)

配上免費的 [letsencrypt](https://letsencrypt.org/) SSL/TLS 證書，一個個安全快速的谷歌又出現了。

<!-- more -->

#### 依賴庫 ####
  1. [`pcre`](http://www.pcre.org/) *正則*
  1. [`ngx_http_proxy_module`](http://nginx.org/en/docs/http/ngx_http_proxy_module.html) *反向代理*
  1. [`ngx_http_substitutions_filter_module`](https://github.com/yaoweibin/ngx_http_substitutions_filter_module) *多重替換*

#### 安裝 ####
> **以 ubuntu 14.04 爲例**
> *i386, x86_64 均適用*

##### 最簡安裝 #####
```bash
#
# 安裝 gcc & git
#
apt-get install build-essential git gcc g++ make

#
# 下載最新版源碼
# nginx 官網:
# http://nginx.org/en/download.html
#
wget "http://nginx.org/download/nginx-1.9.12.tar.gz"

#
# 下載最新版 pcre
# pcre 官網:
# http://www.pcre.org/
#
wget "ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.38.tar.gz"

#
# 下載最新版 openssl
# opessl 官網:
# https://www.openssl.org/
#
wget "https://www.openssl.org/source/openssl-1.0.1s.tar.gz"

#
# 下載最新版 zlib
# zlib 官網:
# http://www.zlib.net/
#
wget "http://zlib.net/zlib-1.2.8.tar.gz"

#
# 下載本擴展
#
git clone https://github.com/cuber/ngx_http_google_filter_module

#
# 下載 substitutions 擴展
#
git clone https://github.com/yaoweibin/ngx_http_substitutions_filter_module


#
# 解壓縮
#
tar xzvf nginx-1.9.12.tar.gz
tar xzvf pcre-8.38.tar.gz
tar xzvf openssl-1.0.1s.tar.gz
tar xzvf zlib-1.2.8.tar.gz

#
# 進入 nginx 源碼目錄
#
cd nginx-1.9.12

#
# 設置編譯選項
#
./configure \
  --prefix=/opt/nginx-1.9.12 \
  --with-pcre=../pcre-8.38 \
  --with-openssl=../openssl-1.0.1s \
  --with-zlib=../zlib-1.2.8 \
  --with-http_ssl_module \
  --with-ipv6 \
  --with-http_v2_module \
  --add-module=../ngx_http_google_filter_module \
  --add-module=../ngx_http_substitutions_filter_module

#
# 編譯, 安裝
# 如果擴展有報錯, 請發 issue 到
# https://github.com/cuber/ngx_http_google_filter_module/issues
#
make
sudo make install

#
# 啓動, 安裝過程到此結束
#
sudo /opt/nginx-1.9.12/sbin/nginx

#
# 配置修改後, 需要 reload nginx 來讓配置生效,
#
sudo /opt/nginx-1.9.12/sbin/nginx -s reload
```

##### 從發行版遷移 #####
``` bash
#
# 安裝 gcc & git
#
apt-get install build-essential git gcc g++ make

#
# 安裝發行版
# (已安裝的請忽略)
#
apt-get install nginx

#
# 查看發行版編譯選項及版本
#
nginx -V
# nginx version: nginx/1.4.7
# built by gcc 4.8.2 (Ubuntu 4.8.2-19ubuntu1)
# TLS SNI support enabled
# configure arguments:
#  --with-cc-opt='-g -O2 -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2' \
#  --with-ld-opt='-Wl,-Bsymbolic-functions -Wl,-z,relro' \
#  --prefix=/usr/share/nginx \
#  --conf-path=/etc/nginx/nginx.conf \
#  --http-log-path=/var/log/nginx/access.log \
#  --error-log-path=/var/log/nginx/error.log \
#  --lock-path=/var/lock/nginx.lock \
#  --pid-path=/run/nginx.pid \
#  --http-client-body-temp-path=/var/lib/nginx/body \
#  --http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
#  --http-proxy-temp-path=/var/lib/nginx/proxy \
#  --http-scgi-temp-path=/var/lib/nginx/scgi \
#  --http-uwsgi-temp-path=/var/lib/nginx/uwsgi \
#  --with-debug \
#  --with-pcre-jit \
#  --with-ipv6 \
#  --with-http_ssl_module \
#  --with-http_stub_status_module \
#  --with-http_realip_module \
#  --with-http_addition_module \
#  --with-http_dav_module \
#  --with-http_geoip_module \
#  --with-http_gzip_static_module \
#  --with-http_image_filter_module \
#  --with-http_spdy_module \
#  --with-http_sub_module \
#  --with-http_xslt_module \
#  --with-mail \
#  --with-mail_ssl_module

#
# 下載對應 nginx 大版本
# nginx 官網:
# http://nginx.org/en/download.html
#
wget "http://nginx.org/download/nginx-1.4.7.tar.gz"

#
# 下載本擴展
#
git clone https://github.com/cuber/ngx_http_google_filter_module

#
# 下載 substitutions 擴展
#
git clone https://github.com/yaoweibin/ngx_http_substitutions_filter_module

#
# 安裝依賴庫的 dev 包
#
apt-get install libpcre3-dev libssl-dev zlib1g-dev libxslt1-dev libgd-dev libgeoip-dev

#
# 請對照自己發行版的 configure 參數進行 configure, 勿直接 copy 以下配置
#
./configure \
  --with-cc-opt='-g -O2 -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2' \
  --with-ld-opt='-Wl,-Bsymbolic-functions -Wl,-z,relro' \
  --prefix=/usr/share/nginx \
  --conf-path=/etc/nginx/nginx.conf \
  --http-log-path=/var/log/nginx/access.log \
  --error-log-path=/var/log/nginx/error.log \
  --lock-path=/var/lock/nginx.lock \
  --pid-path=/run/nginx.pid \
  --http-client-body-temp-path=/var/lib/nginx/body \
  --http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
  --http-proxy-temp-path=/var/lib/nginx/proxy \
  --http-scgi-temp-path=/var/lib/nginx/scgi \
  --http-uwsgi-temp-path=/var/lib/nginx/uwsgi \
  --with-debug \
  --with-pcre-jit \
  --with-ipv6 \
  --with-http_ssl_module \
  --with-http_stub_status_module \
  --with-http_realip_module \
  --with-http_addition_module \
  --with-http_dav_module \
  --with-http_geoip_module \
  --with-http_gzip_static_module \
  --with-http_image_filter_module \
  --with-http_spdy_module \
  --with-http_sub_module \
  --with-http_xslt_module \
  --with-mail \
  --with-mail_ssl_module \
  --with-http_v2_module \
  --add-module=../ngx_http_google_filter_module \
  --add-module=../ngx_http_substitutions_filter_module

#
# 覆蓋二進制文件
#
cp -rf objs/nginx /usr/sbin/nginx

#
# 重啓 nginx 至此, 遷移工作結束
#
service nginx stop
service nginx start

#
# 配置修改後, 需要 restart nginx 來讓配置生效
#
service nginx restart
```

#### 基本配置方法 ####
`http`配置方式

```nginx
server {
  server_name <你的域名>;
  listen 80;

  resolver 8.8.8.8;
  location / {
    google on;
  }
}
```
`https`配置方式
```nginx
server {
  server_name <你的域名>;
  listen 443;

  ssl on;
  ssl_certificate <你的證書路徑>;
  ssl_certificate_key <你的私鑰路徑>;

  resolver 8.8.8.8;
  location / {
    google on;
  }
}
```

> 免費的 letsencrypt 證書簽發教程請移至文章末尾

#### 進階配置方法 ####
##### 基本搜索 #####
需要配置 `resolver` 用於域名解析

```nginx
server {
  # ... 僅列舉部分配置
  resolver 8.8.8.8;
  location / {
    google on;
  }
  # ...
}
```

##### 谷歌學術 #####
`google_scholar` 依賴於 `google`, 所以 `google_scholar` 無法獨立使用.
由於谷歌學術近日升級, 強制使用 `https` 協議, 並且 `ncr` 已經支持, 所以不再需要指定谷歌學術的 `tld`
配置 nginx
``` nginx
location / {
  google on;
  google_scholar on;
}
```

##### 默認語言偏好 #####
默認的語言偏好可用 `google_language` 來設置, 如果沒有設置, 默認使用 `zh-CN` (中文)
```nginx
location / {
  google on;
  google_scholar on;
  # 設置成德文
  google_language "de";
}
```

支持的語言如下.
```txt
ar    -> 阿拉伯
bg    -> 保加利亞
ca    -> 加泰羅尼亞
zh-CN -> 中國 (簡體)
zh-TW -> 中國 (繁體)
hr    -> 克羅地亞
cs    -> 捷克
da    -> 丹麥
nl    -> 荷蘭
en    -> 英語
tl    -> 菲律賓
fi    -> 芬蘭
fr    -> 法國
de    -> 德國
el    -> 希臘
iw    -> 希伯來
hi    -> 印地文
hu    -> 匈牙利
id    -> 印度尼西亞
it    -> 意大利
ja    -> 日本
ko    -> 朝鮮
lv    -> 拉脫維亞
lt    -> 立陶宛
no    -> 挪威
fa    -> 波斯
pl    -> 波蘭
pt-BR -> 葡萄牙 (巴西)
pt-PT -> 葡萄牙 (葡萄牙)
ro    -> 羅馬尼亞
ru    -> 俄羅斯
sr    -> 塞爾維亞
sk    -> 斯洛伐克
sl    -> 斯洛文尼亞
es    -> 西班牙
sv    -> 瑞典
th    -> 泰國
tr    -> 土耳其
uk    -> 烏克蘭
vi    -> 越南
```

##### 搜索引擎爬蟲許可 #####
任何搜索引擎爬蟲都不被允許爬取 google 鏡像
如下的默認 `robots.txt` 已經內置.

```txt
User-agent: *
Disallow: /
```
如果想要使用 `google` 自己的 `robots.txt` 請將 `google_robots_allow` 設爲 `on`

```nginx
  #...
  location / {
    google on;
    google_robots_allow on;
  }
  #...
```

##### Upstreaming #####
`upstream` 減少一次域名解析的開銷, 並且通過配置多個網段的 google ip 能夠一定程度上減少被 google 機器人識別程序偵測到的機率 (彈驗證碼).

```bash
# 可以通過如下方法獲取 google ip
➜  ~  dig www.google.com @8.8.8.8 +short
173.194.38.209
173.194.38.211
173.194.38.212
173.194.38.210
173.194.38.208
```
然後將獲取到的 ip 配置如下即可

``` nginx
upstream www.google.com {
  server 173.194.38.209:443;
  server 173.194.38.211:443;
  server 173.194.38.212:443;
  server 173.194.38.210:443;
  server 173.194.38.208:443;
}
```

##### Proxy Protocal #####
默認採用 `https` 與後端服務器通信.
你可以使用 `google_ssl_off` 來強制將一些域降到 `http` 協議.
這個設置可以讓一些需要二次轉發的域通過 `http` 協議進行轉發, 從而不再依賴 `ssl` 證書.

```nginx
#
# 例如 'www.google.com' 按如下方式代理
# vps(hk) -> vps(us) -> google
#

#
# vps(hk) 配置
#
server {
  # ...
  location / {
    google on;
    google_ssl_off "www.google.com";
  }
  # ...
}

upstream www.google.com {
  server < vps(us) 的 ip >:80;
}

#
# vps(us) 配置
#
server {
  listen 80;
  server_name www.google.com;
  # ...
  location / {
    proxy_pass https://www.google.com;
  }
  # ...
}
```

--------

cloudxns DNS 驗證方式快速獲取 lets-encrypt 證書的 shell 腳本

https://github.com/xdtianyu/scripts/tree/master/le-dns

腳本基於 letsencrypt.sh，通過調用 cloudxns API 更新 TXT 記錄用於認證，實現快速獲取 lets-encrypt 證書。無需 root 權限，無需指定網站目錄及 DNS 解析

下載

```bash
wget https://github.com/xdtianyu/scripts/raw/master/le-dns/le-cloudxns.sh
wget https://github.com/xdtianyu/scripts/raw/master/le-dns/cloudxns.conf
chmod +x le-cloudxns.sh
```

配置

cloudxns.conf 文件內容
```
    API_KEY="YOUR_API_KEY"
    SECRET_KEY="YOUR_SECRET_KEY"
    DOMAIN="example.com"
    CERT_DOMAINS="example.com www.example.com im.example.com"
```

修改其中的 API_KEY 及 SECRET_KEY 爲您的 [cloudxns api key](https://www.cloudxns.net/AccountManage/apimanage.html) ，修改 DOMAIN 爲你的根域名，

修改 CERT_DOMAINS 爲您要籤的域名列表

運行

```bash
./le-cloudxns.sh cloudxns.conf
```

最後生成的文件在當前目錄的 certs 目錄下，然後使用 nginx 加載ssl_certificate字段的fullchain.pem 和 ssl_certificate_key字段的privkey.pem;

cron 定時任務

每兩個月自動更新一次證書，可以在 le-cloudxns.sh 腳本最後加入 service nginx reload 等重新加載服務。

```bash
    * * * */2 * /etc/nginx/le-cloudxns.sh /etc/nginx/le-cloudxns.conf >> /var/log/le-cloudxns.log 2>&1
```

其他問題見 http://v2ex.com/t/260965
