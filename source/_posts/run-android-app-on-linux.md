title: 在Linux上使用Chrome運行Android應用
id: 2523

  
date: 2014-09-22 12:56:02
tags:
---

Google在今年的I/O上發佈了一個移植跨界應用  名爲App Runtime for Chrome（Beta）這個東西可以讓Android系統的App運行於ChromeOS下

目前此功能還在測試 只有幾個官方的Android App公開放出

於是vladikoff大神把這部分國內提取出來 能讓任何系統下的Chrome/Chromium運行Android App

系統需要64位的Chrome/Chromium   版本需要37以上   官方項目說明見[https://github.com/vladikoff/chromeos-apk](https://github.com/vladikoff/chromeos-apk)

&nbsp;

本文適用於Linux/Mac OS X等系統   Windows系統請見 [http://micromacer.lofter.com/post/1c7abf_2677f57](http://micromacer.lofter.com/post/1c7abf_2677f57)

&nbsp;

第一 你先要把Chrome/Chromium的擴展開發者模式打開

並導入vladikoff大神提供的擴展  下載[https://bitbucket.org/vladikoff/archon/get/v1.0.zip](https://bitbucket.org/vladikoff/archon/get/v1.0.zip)  百度網盤:[http://pan.baidu.com/s/1pJCzPzl](http://pan.baidu.com/s/1pJCzPzl)

下載完成 把文件夾解壓縮出來   加載正在開發的擴展程序 選擇此文件夾

![](http://juanzii.me/1/wp-content/uploads/2014/09/installext.png)

&nbsp;

然後官方提供的chromeos-apk   基於node寫的一個工具  用於轉換apk文件   但是在我的linux下面無法使用23333

於是[@farseerfc](http://weibo.com/farseerfc) 告訴我了一個更好的工具  [SimpleLauncher](http://www.reddit.com/r/chromeapks/comments/2gy678/open_source_simplelauncher_has_been_made_many/)（reddit原帖地址）

下載這個壓縮包 [http://www.fileswap.com/dl/4MRne1Fd4F/](http://www.fileswap.com/dl/4MRne1Fd4F/)

和剛纔一樣  把文件夾解壓縮出來  把你要的模擬的apk文件放進vendor/chromium/crx

加載正在開發的擴展程序 選擇此文件夾

這時就有兩個了

![](http://juanzii.me/1/wp-content/uploads/2014/09/installext2.png)

點擊 ch.arnab.simplelauncher的 啓動

![](http://juanzii.me/1/wp-content/uploads/2014/09/simplelauncher.png)

然後點擊你要的app圖標就可以了  並不是所有的apk都可以運行

我這裏測試了一下網易雲音樂

![](http://juanzii.me/1/wp-content/uploads/2014/09/163music-560x1024.png)

輸入法調用的是系統的輸入法  我的這個是搜狗輸入法for linux

![](http://juanzii.me/1/wp-content/uploads/2014/09/usesysinput-838x1024.png)

&nbsp;

播放完全沒有問題  如果你的屏幕是觸摸屏的 可以觸控操作

![](http://juanzii.me/1/wp-content/uploads/2014/09/163music2-558x1024.png)

改變分辨率

改變一下兩個文件的默認值

[gen_main.min.js](https://bitbucket.org/vladikoff/archon/src/master/gen_main.min.js)

[gen_index.min.js](https://bitbucket.org/vladikoff/archon/src/master/gen_index.min.js)

搜索 `tablet: {"long": 1280, "short": 800}`, 修改即可  保存完需要在chrome裏面重新加載(ctrl+r)

&nbsp;

&nbsp;

linux下使用安卓QQ

啓用 chrome://flags 裏面"Native Client Mac, Windows, Linux, Chrome OS, Android"一項 (非常重要)

已經編譯好的QQ  [http://huodong.ustc.edu.cn/static/QQ/com.tencent.mqq.android.crx](http://huodong.ustc.edu.cn/static/QQ/com.tencent.mqq.android.crx)

&nbsp;
