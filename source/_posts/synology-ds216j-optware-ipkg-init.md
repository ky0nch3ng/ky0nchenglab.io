---
title: Synology DS216j Optware IPKG 介紹
date: 2016-09-05 15:47:44
tags: ipkg
---
##### Synology DS216j Optware IPKG 介紹

與 [QNAP TS-219P+](http://jasmin.sakura.ne.jp/blog/0118) 一樣，引入一個 Optware IPKG 作為軟件包管理工具。

DSM 自帶的軟件中心可以安裝需要東西 (比如Git/Subversin/Perl/PHP), 因為已經有了，但你也覺得沒有太大的必要，因為它可能會使用到的機會很少。

![](https://o3ziuzht9.qnssl.com/DS216j_0628_01.png)

##### SSH 服務啟動

在 [Synology DS216j 系統設置](http://jasmin.sakura.ne.jp/blog/0242)中，請必須啟用 SSH 連接服務。

控制面板→終端機和SNMP

![](https://o3ziuzht9.qnssl.com/DS216j_0628_02.png)

* 您可以變更加密模式的安全層級，請注意，安全层级不相容可能導致某些用戶端無法連線。

##### Optware IPKG 安裝

不像 QNAP（現在當我看 QNAP 的 IPKG 已在維護，在後面將不再描述），因為它不能在系統軟件中心裡安裝，只能自己另外安裝。

安裝必須 與 DS216j 的 CPU 架構 （Marvell Armada 385 88F6820） 相符，可以在以下頁面找到這些信息。

[GitHub - trepmag/ds213j-optware-bootstrap: Synology ds213j (manual) optware bootstrap](https://github.com/trepmag/ds213j-optware-bootstrap)

與 Marvell Armada 這個 CPU 相兼容的 optware-bootstrap 似乎不存在、不過與 Marvell Kirkwood 適配的 Marvell Kirkwoodのoptware-bootstrap 相兼容，可以共用。

也就是說,試試看吧。(上述頁面的內容完全相同。)

1. 用 SSH 進行客戶端連接。
2. 先取得 root 權限。

```bash
sudo -i
```

3. 創建用於存放 optware 的根目錄。

```bash
mkdir /volume1/@optware
mkdir /opt
mount -o bind /volume1/@optware /opt
```
4. 設置 IPKG

```bash
feed=http://ipkg.nslu2-linux.org/feeds/optware/cs08q1armel/cross/unstable
ipk_name=`wget -qO- $feed/Packages | awk '/^Filename: ipkg-opt/ {print $2}'`
wget $feed/$ipk_name
tar -xOvzf $ipk_name ./data.tar.gz | tar -C / -xzvf -
mkdir -p /opt/etc/ipkg
echo "src cross $feed" > /opt/etc/ipkg/feeds.conf
```

5. 設置 PATH：/etc/profile (用vi等編輯器)打開、在最後一行下面添加。

```
PATH=/opt/bin:/opt/sbin:$PATH
```

日文原文沒有提到 `source /etc/profile` ，如果不想重啟可以 source 一下。

6. 設置初始化腳本，以便它重新啟動後生效。

 * 以下內容寫入 `/etc/rc.local` 並設置文件的權限為 (chmod 755) 。

```bash
#!/bin/sh

# Optware setup
[ -x /etc/rc.optware ] && /etc/rc.optware start
```

  * 如果之前有共享的 `rc.local` 設置只要複製一下就可以了。

```bash
#將文件從 data 文件夾內複製
cp /volume1/data/rc.local /etc/rc.local
chmod 755 /etc/rc.local
```

 * 以下內容寫入 `/etc/rc.optware` 並設置文件的權限為 (chmod 755) 。

```bash
#!/bin/sh

if test -z "${REAL_OPT_DIR}"; then
# next line to be replaced according to OPTWARE_TARGET
REAL_OPT_DIR=/volume1/@optware
fi

case "$1" in
    start)
        echo "Starting Optware."
        if test -n "${REAL_OPT_DIR}"; then
            if ! grep ' /opt ' /proc/mounts >/dev/null 2>&1 ; then
                mkdir -p /opt
                mount -o bind ${REAL_OPT_DIR} /opt
            fi  
        fi
    [ -x /opt/etc/rc.optware ] && /opt/etc/rc.optware
    ;;
    reconfig)
    true
    ;;
    stop)
        echo "Shutting down Optware."
    true
    ;;
    *)
        echo "Usage: $0 {start|stop|reconfig}"
        exit 1
esac

exit 0
```

  * 如果之前有共享的 `rc.optware` 設置只要複製一下就可以了。

```bash
# 將文件從 data 文件夾內複製
cp /volume1/data/rc.optware /etc/rc.optware
chmod 755 /etc/rc.optware
```

7. IPKG 已經可以使用

```bash
ipkg -v
ipkg version 0.99.163
```

##### Optware IPKG 版本太舊了？

Optware IPKG 在 QNAP一直沒有提供其他的版本，它似乎已經被 Entware 取代。 →[QNAP 4.1.4固件下 GCC 編譯安裝 - Qiita](http://qiita.com/efee/items/5da74eb7e7423e61823c)

而且，Entware-eg（Entware 的另一個版本？）也可以安裝到 Synology 的設備上。

[Install on Synology NAS · Entware-ng/Entware-ng Wiki · GitHub](https://github.com/Entware-ng/Entware-ng/wiki/Install-on-Synology-NAS)

我已經使用 GCC 編譯安裝了 Entware-NG。您也可以嘗試一下。


------

原文鏈接 [http://jasmin.sakura.ne.jp/blog/0244](http://jasmin.sakura.ne.jp/blog/0244)

<!-- more -->

看了這篇文章後我在自己的群暉 216J 上安裝了 `pip`，然後部署了一個 [glances](https://github.com/nicolargo/glances) 用於監控我的 NAS 系統情況。

關於群暉上的一些 python 程序安裝可以看看這個 [http://www.gebi1.com/thread-84122-1-1.html](http://www.gebi1.com/thread-84122-1-1.html)