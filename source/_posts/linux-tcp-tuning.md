title: Linux TCP 性能調優筆記
date: 2017-01-01 17:21:56
tags:
- linux
---
![](https://o3ziuzht9.qnssl.com/linux-bbr.jpg)

本文參考自 [Linux Tuning](http://fasterdata.es.net/host-tuning/linux/)

本文可以幫助您的服務器提高 `并發數量` 和改善 `高延遲掉包` 情況。

目前文中涉及的 hybla 和 htcp 算法已經很火星了，具體釋義請自行 Google 搜索。

本文於 2017 年 1 月 1 日更新加入對 Google TCP BBR 算法的開啟，相關技術解釋見 [知乎](https://www.zhihu.com/question/53559433) 討論

不推薦使用 openvz 虛擬化技術的 VPS 此類權限較低 推薦使用 KVM 架構的虛擬 Linux VPS 。

<!-- more -->

開啟 Google TCP BBR 請先自行將系統內核更新到 Linux Kernel 4.9 版本。


下載 Linux Kernel 4.9 内核

```bash
wget -O linux-image-4.9.0-amd64.deb http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.9/linux-image-4.9.0-040900-generic_4.9.0-040900.201612111631_amd64.deb
```

安裝內核

```bash
dpkg -i linux-image-4.9.0-amd64.deb
```

卸載舊版本內核

```bash
dpkg -l | grep linux-image | awk '{print $2}' | grep -v 'linux-image-4.9.0-040900-generic'|xargs -n1 dpkg --purge
```

更新系統引導和重啟

```bash
gurb update-grub
reboot
```

重新進入系統後查看系統內核版本號

```bash
uname -r
```

寫入 bbr 配置文件

```bash
echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
```

載入配置

```bash
sysctl -p
```

檢查 bbr 是否開啟

```bash
lsmod | grep bbr
```

本方法適用於 [Ubuntu 14.04 LTS](https://wiki.ubuntu.com/LTS)* / 16.04

其他系統開啟方法見 https://teddysun.com/489.html 

--------

內核非 4.9 版本請先啓用 hybla 和 htcp 算法

```bash
/sbin/modprobe tcp_htcp
/sbin/modprobe tcp_hybla
```

然後查看是否啓用成功

```bash
sysctl net.ipv4.tcp_available_congestion_control
```

終端返回以下文字表示啓用成功

```bash
sysctl net.ipv4.tcp_available_congestion_control = htcp cubic reno hybla
```

接著設置  `/etc/sysctl.conf`

```conf
fs.file-max = 51200
#提高整個系統的文件限制
net.ipv4.tcp_syncookies = 1
#表示開啓SYN Cookies。當出現SYN等待隊列溢出時，啓用cookies來處理，可防範少量SYN攻擊，默認爲0，表示關閉；
net.ipv4.tcp_tw_reuse = 1
#表示開啓重用。允許將TIME-WAIT sockets重新用於新的TCP連接，默認爲0，表示關閉；
#表示開啓TCP連接中TIME-WAIT sockets的快速回收，默認爲0，表示關閉；
net.ipv4.tcp_tw_recycle = 0
#爲了對NAT設備更友好，建議設置爲0。
#修改TW快速回收的問題以更好的兼容移動設備。
net.ipv4.tcp_fin_timeout = 30
#修改系統默認的 TIMEOUT 時間。
net.ipv4.tcp_keepalive_time = 1200
#表示當keepalive起用的時候，TCP發送keepalive消息的頻度。缺省是2小時，改爲20分鐘。
net.ipv4.ip_local_port_range = 10000 65000 #表示用於向外連接的端口範圍。缺省情況下很小：32768到61000，改爲10000到65000。（注意：這裏不要將最低值設的太低，否則可能會佔用掉正常的端口！）
net.ipv4.tcp_max_syn_backlog = 8192
#表示SYN隊列的長度，默認爲1024，加大隊列長度爲8192，可以容納更多等待連接的網絡連接數。
net.ipv4.tcp_max_tw_buckets = 5000
#表示系統同時保持TIME_WAIT的最大數量，如果超過這個數字，TIME_WAIT將立刻被清除並打印警告信息。
#額外的，對於內核版本新於**3.7.1**的，我們可以開啓tcp_fastopen：
net.ipv4.tcp_fastopen = 3
increase TCP max buffer size settable using setsockopt()
net.core.rmem_max = 67108864
net.core.wmem_max = 67108864
increase Linux autotuning TCP buffer limit
net.ipv4.tcp_rmem = 4096 87380 67108864
net.ipv4.tcp_wmem = 4096 65536 67108864
increase the length of the processor input queue
net.core.netdev_max_backlog = 250000
recommended for hosts with jumbo frames enabled
net.ipv4.tcp_mtu_probing=1
#美帝VPS推薦設置TCP擁塞算法爲 hybla 算法， 十一區VPS推薦設置 htcp 算法
net.ipv4.tcp_congestion_control=htcp
```

設置文件寫完后執行

```bash
sysctl -p
```

Enjoy~

各算法優劣對比請見 CSDN 大神的 [分析](http://blog.csdn.net/zhangskd/article/details/6715751)

本文參考 https://github.com/iMeiji/shadowsocks_install/wiki/%E5%BC%80%E5%90%AFTCP-BBR%E6%8B%A5%E5%A1%9E%E6%8E%A7%E5%88%B6%E7%AE%97%E6%B3%95