title:  Windows 10 技巧筆記
date: 2015-11-28 16:10:23
tags: Windows
---
![](http://static.cnbetacdn.com/thumb/article/2016/0803/e611861082ffe37.jpg_600x600.jpg)

近期 M$ 正式向 Windows 10 用戶推送了 Redstone 更新，也就是傳說中的 Anniversary Update。（微軟改名部工作真多，我只想問一句，你們還缺人嗎？我來幫你們想名字。

平時也只是用 Windows 玩玩遊戲，在使用的這個新系統的時候覺得還是有些地方要去修改的。

接下來，我要開始裝逼了。

<!-- more -->

![](https://o3ziuzht9.qnssl.com/win10ad1.jpg)
<div align=right> <sup>臺灣地區的 Win10 廣告</sup></div>

![](https://o3ziuzht9.qnssl.com/win10ad2.jpg)
<div align=right> <sup>大陸地區的 Win10 廣告</sup></div>

微軟的 i18n 做的和水果半斤八兩，坐和放寬，開了又開。

M$ 之後更新了  大補之後的完整 ISO 鏡像下載

[Win10 1607 正式版64位英文版（含家庭版、专业版）](ed2k://|file|en_windows_10_multiple_editions_version_1607_updated_jul_2016_x64_dvd_9058187.iso|4380387328|870E0589EBEC3296745462E8ACA53FB2|/)

SHA1：99FD8082A609997AE97A514DCA22BECF20420891

来自 [MSDN订阅](http://t.cn/R529U7U)

------

開機之後發現鎖屏時數字小鍵盤沒有自動開

可以修改註冊表解決 另存爲bootNumberLock.reg 管理員運行權限執行

  Windows Registry Editor Version 5.00
```
[HKEY_USERS\.DEFAULT\Control Panel\Keyboard]
"InitialKeyboardIndicators"="80000002"
```

在 Windows8 / 8.1 /10 下，其數值爲：80000002 （在 Windows 7 中數值取十六進制值最後一位爲：2

80000000 – 關閉全部鎖定 (包括 NumLock, CapsLock, ScrollLock)

80000001 – 開啓  CapsLock

80000002 – 開啓 NumLock

80000003 – 開啓 CapsLock 和 NumLock

80000004 – 開啓 ScrollLock

80000005 – 開啓 CapsLock 和 ScrollLock

80000006 – 開啓 NumLock 和 ScrollLock

80000007 – 開啓全部鎖定 (包括 NumLock, CapsLock, ScrollLock)

這些具體數值 M$ 的 TechNet 也偷懶不寫 https://technet.microsoft.com/en-us/library/cc978657.aspx 輪子哥就知道整天刷知乎。。。

此方法僅限 BIOS/EFI/UEFI 開啓鍵盤功能的情況下

其他解決方法可以試試看下面這個（懶得翻，自己看。

1. If the above doesn’t work, maybe you have to set the NumLock state to ON in BIOS settings. So, you have to enter into your BIOS and change the NumLock state to ON. To do that:

1. Power On your computer and press “DEL” or “F1” or “F2” or “F10” to enter BIOS (CMOS) setup utility. (The way to enter into BIOS Settings depends on the computer manufacturer).
2. Inside BIOS find the POST Behavior menu.
3. Set the NumLock key (state) option to ON.
4. Save and exit from BIOS settings.

2. In some HP laptops the only way to keep NumLock ON, is to search for all “InitialKeyboardIndicators” strings with numeric values inside the registry and set the value to “2147483650” (without quotes). To do that:

1. Open Registry Editor.
2. From main menu click Edit and choose Find.
3. Inside Find What box, type InitialKeyboardIndicators and click Find Next.
4. Change the value data to “2147483650” in every “InitialKeyboardIndicators” string inside registry, by using the F3 key to find all of them.

--------------------

Windows 10 聚焦壁紙路徑

```
%localappdata%\Packages\Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy\LocalState\Assets

ren *.* *.jpg
```

---------

然後去除桌面的快捷方式小箭頭

批處理腳本解決 另存爲 clearArrow.bat 管理員運行權限執行

```
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Icons" /v 29 /d "%systemroot%\system32\imageres.dll,197" /t reg_sz /f
taskkill /f /im explorer.exe
attrib -s -r -h "%userprofile%\AppData\Local\iconcache.db"
del "%userprofile%\AppData\Local\iconcache.db" /f /q
start explorer
pause
```

恢復小箭頭也可以批處理腳本解決 另存爲 recArrow.bat 管理員運行權限執行

```
reg delete "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Icons" /v 29 /f
taskkill /f /im explorer.exe
attrib -s -r -h "%userprofile%\AppData\Local\iconcache.db"
del "%userprofile%\AppData\Local\iconcache.db" /f /q
start explorer
pause
```

---------------

更新之後發現標題欄主題色也回隨之變化，如果想回到一直白色背景的狀態可以存爲 win10WhiteThemeREC.reg 管理員運行權限執行

```
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM]
"ColorPrevalence"=dword:00000000
```

恢復可以存爲 win10ColorThemeREC.reg 管理員運行權限執行

```
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM]
"ColorPrevalence"=dword:00000001
```

然後我發現了更簡單的方法

勾選 『顯示開始“菜單”、 任務欄、操作中心和標題欄的顏色 』選項。。。。即可

![](https://o3ziuzht9.qnssl.com/win10colorsetting.png)

-----

![](https://o3ziuzht9.qnssl.com/win10_open_dev.png)

打開系統開發者選項，安裝 Bash for Widnows 功能

接着通過系統自帶搜索鍵入“Windows 功能”，進入“啓用或關閉 Windows 功能”頁面，在提供的功能列表中，開啓“Windows Subsystem for Linux (Beta)”，之後打開命令提示符，輸入“bash”回車，系統就是自動設置了。

卸載子系統和重裝

```
lxrun /uninstall /full 
lxrun /install
```
-----

GTA 全系列用戶音樂軟鏈接
```
cd C:\Users\%USERNAME%\Documents\Rockstar Games\GTA V\User Music
for /r "E:\MusicPath" %I in (*.mp3) do mklink "%~nxI" "%I"
```

---------

時間同步

設定要同步的 NTP Server 

```
w32tm /config /update /manualpeerlist:cn.pool.ntp.org
```

開始進行同步 

```
w32tm /resync
```

開機自動同步 NTP 時間

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\W32Time\TimeProviders\NtpClient]
"SpecialPollInterval"=dword:0000003c
```

-----

重建系統圖標緩存

```
ie4uinit.exe -show 

taskkill /IM explorer.exe /F 

DEL /A /Q "%localappdata%\IconCache.db" 

DEL /A /F /Q "%localappdata%\Microsoft\Windows\Explorer\iconcache*"

shutdown -l
```

-----
時間同步 UTC 格式

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation]
"RealTimeIsUniversal"=dword:00000001
```

-----

然後改善 Windows 10 的中文字體顯示，看慣了 Mac OS X 的字體渲染，M$的這個渣優化簡直是瞎眼，坐等微軟推送盲文補丁。。。

簡體中文字體渲染很爛，繁體字體渲染更爛。

還好大神給出了字體補丁包 http://pan.baidu.com/s/1ntCdh6h

然後用 [字體替換工具](http://www.fishlee.net/soft/SysFontReplacer/) 解決。

可以下載覆蓋修復，微軟截至本文發佈時並沒有解決字體問題的方案。

反正 Windows 也主要作爲我的“遊戲機”系統，平常大部分作業還是在 Mac OS X 下完成的，我也習慣了 Unix/Linux 的操作方式了（裝完B快逃

[進階指南](http://sspai.com/34288)

另外牆裂推薦 [Visual Studio Code](https://ky0n.xyz/visualstudio-code/) 替代系統自帶的文本編輯器。
