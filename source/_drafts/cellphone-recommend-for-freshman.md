title: 适合大学新生的手机推荐
id: 1885

  - 日志
date: 2014-07-10 02:59:20
tags:
---

又快到九月份了，刚刚高考完的高三学生即将进入大学校园，是时候换个新手机了 。 作为一个大专狗 手机需要的功能也就那么几个 打电话发短信（这是废话） 、聊QQ微信刷微博刷人人刷空间刷贴吧、听歌、看电影（你懂的）、 玩手机游戏（不过天朝本土的手机游戏我都不玩 质量画面太美不敢玩 推荐supercell的coc和boom beach） 其他也没什么功能必须了 但是要玩游戏 看电影流畅 必须配置跟得上 所以推荐谷歌的两个亲儿子 Nexus 4和Nexus 5 都由LG代工 作为看家本领 屏幕的显示效果真的不错 而且整机比较便宜

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/nexus41-154x300.jpg)

Nexus 4

屏幕 触摸屏类型电容屏，多点触控 主屏尺寸4.7英寸 主屏材质True HD IPS 主屏分辨率1280×768像素 屏幕像素密度318ppi 屏幕技术康宁大猩猩玻璃 窄边框3.64mm 屏幕占比68.35% 网络 网络类型单卡双模 3G网络联通3G（WCDMA&amp; HSPA+），联通2G/移动2G（GSM） 支持频段2G：GSM 850/900/1800/1900 3G：WCDMA 900/2100MHz WLAN功能WIFI，IEEE 802.11 n/b/g 导航GPS导航，A-GPS技术 连接与共享NFC，无线充电，DLNA，WLAN热点，蓝牙4.0 硬件 操作系统Android OS 4.4.4（永远是最新的Andorid版本 2014年秋天推送Android L 5.0 因为是谷歌亲儿子 第三方ROM也很丰富） 核心数四核 CPU型号高通 骁龙Snapdragon APQ8064 CPU频率1536MHz GPU型号高通 Adreno320 RAM容量2GB ROM容量8GB或者16GB 存储卡不支持容量扩展 电池类型不可拆卸式电池 电池容量2100mAh （智能手机基本都是一天一充） 理论通话时间918分钟 理论待机时间390小时 摄像头 摄像头内置 摄像头类型双摄像头（前后） 后置摄像头像素800万像素 前置摄像头像素130万像素 传感器类型CMOS 闪光灯LED补光灯 光圈f/2.4 视频拍摄支持 外观 造型设计直板 机身颜色黑色，白色（这里的颜色指的是背壳颜色 前面都是黑色的） 手机尺寸133.9×68.7×9.1mm 手机重量139g 操作类型触控按键 感应器类型重力感应器，光线传感器，距离传感器 SIM卡类型Micro SIM卡 纠错 机身接口3.5mm耳机接口，Micro USB v2.0数据接口（不支持OTG 但可以通过刷内核和外部供电实现此功能）

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/nexus51-152x300.jpg)

Nexus 5

屏幕 触摸屏类型电容屏，多点触控 主屏尺寸4.95英寸 主屏材质IPS 主屏分辨率1920×1080像素 屏幕像素密度445ppi 屏幕技术康宁大猩猩玻璃，OGS全贴合技术 窄边框3.77mm 屏幕占比70.84% 网络 3G网络联通3G（WCDMA&amp; HSPA+），联通2G/移动2G（GSM） 支持频段2G：GSM 850/900/1800/1900 3G：WCDMA 850/900/1900/2100MHz （破解后支持电信3G cdma 2000 evdo 我是连接你懂的）

4G:美版（D820）Nexus 5：

(说人话就是美版支持中国移动,中国电信和中国联通的2.6g TDD-LTE

不支持联通FDD-LTE 我朝FDD规则未出台完毕 本人也不清楚 由于频段比国际版丰富 故推荐入美版）

GSM： 850/900/1800/1900 MHz

CDMA: Band Class: 0/1/10

WCDMA: Bands: 1/2/4/5/6/8/19

LTE:Bands:1/2/4/5/17/19/25/26/41

国际版（日版港版欧版 ect.）（D821）Nexus 5：

（支持联通4G FDD-LTE、电信4G FDD-LTE 不支持移动4G TDD-LTE)

GSM：850/900/1800/1900 MHz

WCDMA: Bands: 1/2/4/5/6/8

LTE: Bands: 1/3/5/7/8/20

WLAN功能WIFI，IEEE 802.11 a/n/b/g 导航GPS导航，A-GPS技术 连接与共享NFC，无线充电，DLNA，WLAN热点，蓝牙4.0 硬件 操作系统Android OS 4.4.4（永远是最新的Andorid版本 2014年秋天推送Android L 5.0 同样是谷歌亲儿子 第三方ROM也很丰富） 核心数四核 CPU型号高通 骁龙800（MSM8974） CPU频率2355MHz GPU型号高通 Adreno330 RAM容量2GB ROM容量16GB 存储卡不支持容量扩展 电池容量2300mAh（智能手机基本都是一天一充） 摄像头 摄像头内置 摄像头类型双摄像头（前后） 后置摄像头像素800万像素 前置摄像头像素130万像素 传感器类型CMOS 闪光灯LED补光灯 光圈f/2.4 拍照功能曝光补偿，面部检测，白平衡，HDR，全景模式，延时自拍，连拍，自动对焦 外观 造型设计直板 机身颜色黑色，白色，红色（这里的颜色指的是背壳颜色 前面都是黑色的 女孩子可以买红色版本的） 手机尺寸137.84×69.17×8.59mm 手机重量130g 纠错 操作类型虚拟按键 感应器类型重力感应器，光线传感器，距离传感器，陀螺仪 SIM卡类型Micro SIM卡 机身接口3.5mm耳机接口，Micro USB v2.0数据接口（支持OTG）

* * *

上面是亲儿子 下面介绍的是干儿子（联想已经从Google收购MOTO 但未交接完成 现还属于Google旗下） 现在moto x也降的差不多了 自己查下X宝的价钱

moto x

屏幕 触摸屏类型电容屏，多点触控 主屏尺寸4.7英寸 主屏材质Super AMOLED 主屏分辨率1280×720像素 屏幕像素密度312ppi 窄边框3.39mm 屏幕占比72.13% 网络 网络类型单卡多模 4G网络LTE 3G网络联通3G（WCDMA），联通2G/移动2G（GSM） 支持频段2G：GSM 850/900/1800/1900 3G：WCDMA 850/1700/1900/2100MHz WLAN功能WIFI，IEEE 802.11 a/n/b/g/ac 导航GPS导航，A-GPS技术，GLONASS导航 连接与共享NFC，DLNA，WLAN热点，蓝牙4.0 硬件 操作系统Android OS 4.2 （剁手的官方推送很坑爹 推荐刷第三方rom 刷什么 后文我会介绍的） 核心数双核 CPU型号高通 骁龙Snapdragon MSM8960T CPU频率1741MHz GPU型号高通 Adreno320 RAM容量2GB ROM容量16GB（可定制32GB） 存储卡不支持容量扩展 电池类型不可拆卸式电池 电池容量2200mAh 摄像头 摄像头内置 摄像头类型双摄像头（前后） 后置摄像头像素1000万像素 前置摄像头像素200万像素 传感器类型CMOS 闪光灯LED补光灯 外观 造型设计直板 机身颜色黑色，白色 手机尺寸129.3×65.3×10.4mm 手机重量130g 机身特点第三代康宁大猩猩强化玻璃 操作类型虚拟按键 三防功能支持 感应器类型重力感应器，光线传感器，距离传感器 SIM卡类型Nano SIM卡 机身接口3.5mm耳机接口，Micro USB v2.0数据接口

另外moto g也很实惠 ![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/motog1-154x300.jpg)

moto g

屏幕 触摸屏类型电容屏，多点触控 主屏尺寸4.5英寸 主屏材质TFT材质（IPS技术） 主屏分辨率1280×720像素 屏幕像素密度326ppi 屏幕技术康宁大猩猩玻璃 窄边框4.93mm 屏幕占比65.22% 网络 网络类型双卡（港版），单卡（美版） 3G网络联通3G（WCDMA），联通2G/移动2G（GSM） 支持频段2G：GSM 850/900/1800/1900 3G：WCDMA 850/1700/1900/2100MHz WLAN功能IEEE 802.11 n/b/g 导航GPS导航，A-GPS技术，GLONASS导航 连接与共享DLNA，WLAN热点，蓝牙4.0+A2DP 硬件 操作系统Android OS 4.3 核心数四核 CPU型号高通 骁龙Snapdragon MSM8226 CPU频率1228MHz GPU型号高通 Adreno305 RAM容量1GB ROM容量8GB 电池容量2070mAh 摄像头 摄像头内置 摄像头类型双摄像头（前后） 后置摄像头像素500万像素 前置摄像头像素130万像素 传感器类型CMOS 闪光灯LED补光灯 外观 造型设计直板 机身颜色黑色，白色，绿色，粉色 手机尺寸129.9×65.9×11.6mm 手机重量143g 三防功能支持 感应器类型重力感应器，光线传感器，距离传感器 纠错 机身接口3.5mm耳机接口，Micro USB v2.0数据接口

* * *

不推荐

所有国产手机 性价比不高 比如中（中兴ZTE：呵呵）华（华为huawei 菊花一朵）酷（酷派coolpad [我才是三丧！](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/9LRGFVPJ43UD0031.jpg)）联 （联想 科比代言的菜刀卖了几把？） 也没有几个用心在做产品的 例如我朝菊花的[版本](http://www.ithome.com/html/android/93375.htm)门 粮食的“天王盖地虎 小米双负五”（你是买不到的 别瞎想了 要买到加钱吧！不过小米的miui确实强大 已经不是当年的based CM了） 魅族的老板就是喷子 不过MX3是很好看 但是坚持反人类的[sb底栏](http://android.tgbus.com/lab/hardware/201212/458723.shtml) flyme系（an）统（zhuo） 锤子用心是用心了 ceo的单口相声我也很喜欢 产品怎么说呢 拍照效果挺不错的系统UI也是深度定制（系统基于CM所以稳定）很漂亮 oppo和一加一个模子 你懂的 不过一加相当于加强版的nexus 5 也不错 （1999.99这个价钱也对得起系统优化的效果了）

以下厂商不推荐买国行

HTC 火腿肠这几年的机海战术用尽 这几年的HTC butterfly，HTC M7,HTC M8的工艺设计确实顶尖 但是售价你懂←_← 看看就行了 今年火腿肠想通了 出了个[HTC DESIRE 816](http://detail.zol.com.cn/cell_phone/index370154.shtml) 还可以 良心价 （台湾是中国的不可分割一部分23333）

Asus 阿苏斯华硕 近的[zone phone 5](http://www.asus.com/tw/Phones/ASUS_ZenFone_5/) 走中端路线 设计也挺好看的 喜欢就买买买 （同台湾厂家）

SONY大法 旗舰[SONY Z2](http://www.sonymobile.com/global-zh/products/phones/xperia-z2/) 确实NB 索尼大。法好！买买买

* * *

选择Android安卓手机 就是要选个性价比高的 耐玩耐用（cao）的 所以系统的流畅稳定很重要 之所以本人强力推荐谷歌亲儿子 就因为它能获得猴机（android戏称安德猴 为什么？自己查这货的德文发音）的最好体验

原生安卓并不是很适合国人使用 我建议大家可以刷第三方ROM 刷之前必须解除BootLoader的锁（人为解锁视为放弃保修） 但我推荐的都是国外版本 反正中国境内也没地方给你保 一般操作是很难损坏硬件的（三丧除外 为什么？自行搜索字库门） 解完bl锁 然后root 用一些什么刷机大师 刷机精灵 无压力root之。

然后就可以选择刷自己喜欢的rom系统 下面我介绍一些著名的第三方安卓rom

小米的[MIUI](http://www.miui.com/) 主题丰富 随便你折腾 基于安卓4.2 基于4.4的正在测试中

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/miui1-1024x484.jpg)

乐蛙[lewaos](http://lewaos.com) 也是一个主题丰富的国产安卓系统 类似楼上 大部分基于安卓4.2 个别4.4

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/lewaos1.jpg)

[Cyanogenmod](http://www.cyanogenmod.org/) 全球最大的Android第三方编译团队 出品的rom适配多种主流机型 稳定流畅著名 系统样式是谷歌原生风格的UI CM11基于最新的安卓4.4.4

下载时请选择手机的开发代号 比如Nexus4的开发代号是mako （页面内ctrl+f 基本就能搜到） ![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/cm111.jpg)

魔趣 [Mokee](http://www.mokeedev.com/) 国产的优秀rom 属于非营利性民间玩家论坛 系统稳定流畅 优化适合果然使用 UI风格和cm一样是谷歌原生安卓的 （本人使用的也是这款rom谁用谁知道）

下载时请选择手机的开发代号 比如Nexus4的开发代号是mako （页面内ctrl+f 基本就能搜到）

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/mokee1-1024x428.jpg)

[Aokp](http://aokp.co/) 最激进的安卓rom UI风格和cm一样是谷歌原生安卓的 每天都在更新新功能

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/aokp1.jpg)

Aospa 又叫[Paranoid Android](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/ParanoidAndroid_4.421-1024x640.jpg) 平板、手机模式基于一体 UI风格和cm一样是谷歌原生安卓的 紧跟谷歌设计风格 ![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/ParanoidAndroid_4.421-1024x640.jpg)

* * *

以下产品为土豪新生专用 非战斗人员请立即撤离！

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/iphone5s1-142x300.jpg) iPhone 5S

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/iphone5c1-143x300.jpg) iPhone 5C

选择iPhone的 请你忘记参数 因为没有任何意义 系统UI很漂亮简洁 运行很流畅稳定 app商店的应用数量很多 我们猴机（前文我解释过了）的很多游戏都是iOS移植来的

有充足的预算就选择iPhone 5S 喜欢彩壳就选择iPhone 5C（女生推荐） 本人不推荐iPhone 5 因为这个手机作为富士康最难生产的手机 现在也已经停产 市面上翻新机很多 很难鉴别真机或翻新

这两个厂的产品就不用考虑了 一个倒闭之后被收购了 大部分产品还采用M$的反人类的手机系统 主流系统的手机又是万年512MB/ 768MB的“古董机” ，一个万年产奇葩屏幕的 质量差如狗。

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/antinokia1.png)

![](http://ippoblog.qiniudn.com/wp-content/uploads/2014/07/antisamsung1.png)

![](http://i.creativecommons.org/l/by-sa/4.0/88x31.png) 本文采用 署名-相同方式共享 3.0 许可协议进行许可。 This site is licensed under a Creative Commons Attribution 3.0 Unported License.